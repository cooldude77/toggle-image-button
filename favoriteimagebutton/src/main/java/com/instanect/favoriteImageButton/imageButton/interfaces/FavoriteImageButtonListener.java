package com.instanect.favoriteImageButton.imageButton.interfaces;

import android.view.View;

public interface FavoriteImageButtonListener {

    void onConnectionFavoriteImageButtonClicked(View v);
}

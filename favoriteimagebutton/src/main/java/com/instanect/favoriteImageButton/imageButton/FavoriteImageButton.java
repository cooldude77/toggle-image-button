package com.instanect.favoriteImageButton.imageButton;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageButton;

import com.instanect.favoriteImageButton.imageButton.interfaces.FavoriteImageButtonListener;

public class FavoriteImageButton extends AppCompatImageButton {
    FavoriteImageButtonListener favoriteImageButtonListener;
    boolean isFavorite;

    public FavoriteImageButton(Context context) {
        super(context);
    }

    public FavoriteImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FavoriteImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setFavoriteImageButtonListener(FavoriteImageButtonListener favoriteImageButtonListener) {
        setOnClickListener(favoriteImageButtonListener::onConnectionFavoriteImageButtonClicked);
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

}

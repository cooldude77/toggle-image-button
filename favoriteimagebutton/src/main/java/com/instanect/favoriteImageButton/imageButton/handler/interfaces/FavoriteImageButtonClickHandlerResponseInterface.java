package com.instanect.favoriteImageButton.imageButton.handler.interfaces;

import android.view.View;

public interface FavoriteImageButtonClickHandlerResponseInterface {

    void onFavoriteImageButtonClicked(View v, boolean isFavoriteInCurrentState);
}

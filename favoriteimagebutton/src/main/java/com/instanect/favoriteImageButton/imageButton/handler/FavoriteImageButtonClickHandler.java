package com.instanect.favoriteImageButton.imageButton.handler;

import android.content.Context;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.instanect.favoriteImageButton.imageButton.FavoriteImageButton;
import com.instanect.favoriteImageButton.imageButton.handler.interfaces.FavoriteImageButtonClickHandlerResponseInterface;
import com.instanect.favoriteImageButton.imageButton.interfaces.FavoriteImageButtonListener;

/**
 * Handler handles the images
 * Calls the interface method for external processing
 * before the toggle happens
 * The caller then confirmsToggle in order to tell the handler that
 * button can change the state ( set the flag and the drawable)
 * <p>
 * The button too calls the handler upon click
 * <p>
 * Impl:
 * - first
 * favoriteImageButton.setIsFavorite(output from logic);
 * - Connect button to this
 * favoriteImageButton.setFavoriteImageButtonListener(handler);
 * - Connect response to handler
 * handler.setFavoriteImageButtonClickHandlerResponseInterface(responseInterface);
 * on click responseInterface is called with current status
 * <p>
 * responseInterface calls handler.confirmToggle() to confirm or not to let it lapse
 * <p>
 * Handler changes image and tells button to update its state
 * Process over
 */
public class FavoriteImageButtonClickHandler implements FavoriteImageButtonListener {

    private Context context;
    private int imageOnSetFavorite;
    private int imageOnRemoveFavorite;
    private FavoriteImageButton favoriteImageButton;
    private FavoriteImageButtonClickHandlerResponseInterface clickHandlerResponseInterface;

    public FavoriteImageButtonClickHandler(
            Context context,
            int imageOnSetFavorite,
            int imageOnRemoveFavorite) {
        this.context = context;
        this.imageOnSetFavorite = imageOnSetFavorite;
        this.imageOnRemoveFavorite = imageOnRemoveFavorite;
    }

    public void setClickHandlerResponseInterface(FavoriteImageButtonClickHandlerResponseInterface clickHandlerResponseInterface) {
        this.clickHandlerResponseInterface = clickHandlerResponseInterface;
    }

    private void setFavoriteImage() {

        if (favoriteImageButton.isFavorite())
            favoriteImageButton.setImageDrawable(
                    ContextCompat.getDrawable(context, imageOnSetFavorite));
        else
            favoriteImageButton.setImageDrawable(
                    ContextCompat.getDrawable(context, imageOnRemoveFavorite));
    }

    public void setFavoriteImageButton(FavoriteImageButton favoriteImageButton) {

        this.favoriteImageButton = favoriteImageButton;
        favoriteImageButton.setFavoriteImageButtonListener(this);
    }

    @Override
    public void onConnectionFavoriteImageButtonClicked(View v) {
        if (clickHandlerResponseInterface != null)
            clickHandlerResponseInterface.onFavoriteImageButtonClicked(
                    v,
                    favoriteImageButton.isFavorite());
        else
            confirmToggle();

    }

    public void confirmToggle() {
        favoriteImageButton.setIsFavorite(
                !favoriteImageButton.isFavorite()
        );

        setFavoriteImage();
    }

}

package com.instanect.favimagebutton

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.instanect.favoriteImageButton.imageButton.handler.FavoriteImageButtonClickHandler
import com.instanect.favoriteImageButton.imageButton.handler.interfaces.FavoriteImageButtonClickHandlerResponseInterface
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), FavoriteImageButtonClickHandlerResponseInterface {

    private lateinit var handler: FavoriteImageButtonClickHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handler =
            FavoriteImageButtonClickHandler(
                this,
                R.drawable.ic_star_filled,
                R.drawable.ic_star_white
            )

        favoriteImageButton.setIsFavorite(true)
        handler.setFavoriteImageButton(favoriteImageButton)

        handler.setClickHandlerResponseInterface(this)

    }

    override fun onFavoriteImageButtonClicked(v: View?, isFavoriteInCurrentState: Boolean) {
        Toast.makeText(this, "Some Process", Toast.LENGTH_SHORT).show()

        handler.confirmToggle()
    }
}
